<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../Config/Database.php';
include_once '../Classes/PayOut.php';

$database = new Database();
$db = $database->getConnection();

$payout = new Payout($db);

$data = json_decode(file_get_contents("php://input"));

if (
    !empty($data->Id)
) {
    $payout->Id = $data->Id;
    $payout->UpdatedAt = date('Y-m-d H:i:s');
    if ($payout->updateStatus($db)) {
        http_response_code(201);
        echo json_encode(array("message" => "payout sucessfull."));
    } else {
        http_response_code(503);
        echo json_encode(array("message" => "Unable to make payout."));
    }
} else {
    http_response_code(400);
    echo json_encode(array("message" => "Unable to make payout. Data is incomplete."));
}
