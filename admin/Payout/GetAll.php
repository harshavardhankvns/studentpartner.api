<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
include_once '../Config/Database.php';
include_once '../Classes/PayOut.php';

$database = new Database();
$db = $database->getConnection();

$payout= new PayOut($db);

$result = $payout->GetAll($db);
    $payouts=array();
	while ($payout = $result->fetch_assoc()) {
       array_push($payouts, $payout);
    }    
    http_response_code(200);     
    echo json_encode($payouts);
