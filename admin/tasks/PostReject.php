<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../Config/Database.php';
include_once '../Classes/Task.php';
include_once '../Classes/Coin.php';

$database = new Database();
$db = $database->getConnection();

$task = new Task($db);
$coin = new Coin($db);

$data = json_decode(file_get_contents("php://input"));

if (
    !empty($data->TaskId) &&
    !empty($data->Reason) &&
    !empty($data->Status)
) {
    $task->Status = $data->Status;
    $task->UpdatedAt = date('Y-m-d H:i:s');
    $task->Id = $data->TaskId;
    $task->Reason = $data->Reason;
    if ($task->UpdateRejectStatus($db)) {
        http_response_code(201);
        echo json_encode(array("message" => "Task rejected."));
    } else {
        http_response_code(503);
        echo json_encode(array("message" => "Unable to reject Task."));
    }
} else {
    http_response_code(400);
    echo json_encode(array("message" => "Unable to reject Task. Data is incomplete."));
}
