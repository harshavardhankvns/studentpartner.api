<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
include_once '../Config/Database.php';
include_once '../Classes/Task.php';

$database = new Database();
$db = $database->getConnection();

$task= new Task($db);

$result = $task->GetAll($db);
if($result->num_rows > 0){    
    $taskRecords=array();
	while ($task = $result->fetch_assoc()) { 	
        $taskDetails=$task; 
       array_push($taskRecords, $taskDetails);
    }    
    http_response_code(200);     
    echo json_encode($taskRecords);
}else{     
    http_response_code(404);     
    echo json_encode(
        array("message" => "No task found.")
    );
} 
