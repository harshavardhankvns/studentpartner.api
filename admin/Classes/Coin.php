<?php
class Coin
{
    public $id;
    public $taskId;
    public $studentPartnerId;
    public $coinsRewarded;
    public $createdAt;
    public $updatedAt;
    private $coinsTable = "coins";
    private $conn;

    public function __construct($db)
    {
    }
    function create($db){
        $this->taskId = htmlspecialchars(strip_tags($this->taskId));
        $this->studentPartnerId = htmlspecialchars(strip_tags($this->studentPartnerId));
        $this->coinsRewarded = htmlspecialchars(strip_tags($this->coinsRewarded));
        $this->createdAt = htmlspecialchars(strip_tags($this->createdAt));

        $stmt = $db->prepare(
            "INSERT INTO ".$this->coinsTable."(
                   `TaskId`,
                   `studentPartnerId`,
                   `CoinsRewarded`,
                   `CreatedAt`)
                   VALUES(
                       '".$this->taskId."',
                       '".$this->studentPartnerId."',
                       '".$this->coinsRewarded ."',
                       '".$this->createdAt."')");
		
		if($stmt->execute()){
			return true;
		}
	 
		return false;		 
    }
}
