<?php
class PayOut
{
    public $Id;
    public $studentPartnerId;
    public $type;
    public $amount;
    public $status;
    public $CreatedAt;
    public $UpdatedAt;
    private $payoutTable = "payout";
    private $conn;

    public function __construct($db)
    {
    }

    function GetAll($db)
    {
        $stmt = $db->prepare("SELECT P.*, SP.Name, SP.UPIId FROM payout P JOIN studentpartners SP ON SP.Id = P.StudentPartnerId WHERE P.Status = 0 ORDER BY P.CreatedAt DESC");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    function updateStatus($db)
    {
		$this->Id = htmlspecialchars(strip_tags($this->Id));

		$stmt = $db->prepare("
			UPDATE payout
			SET 
			Status = 1,
            UpdatedAt = '$this->UpdatedAt'
			WHERE Id = '$this->Id'");
		if($stmt->execute()){
			return true;
		}
	 
		return false;
    }

    
}