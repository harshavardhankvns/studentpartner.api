<?php
class Task
{

	private $tasksTable = "tasks";
	public $Id;
	public $TaskTypeId;
	public $StudentPartnerId;
	public $Reason;
	public $ConductedOn;
	public $SessionUrl;
	public $SessionImageUrl;
	public $Message;
	public $Status;
	public $CreatedAt;
	public $UpdatedAt;
	private $conn;

	function GetAll($db)
	{
		$stmt = $db->prepare("SELECT T.*, C.CoinsRewarded FROM " . $this->tasksTable . " T LEFT JOIN coins C ON T.Id = C.TaskId  ORDER BY T.CreatedAt DESC");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result;
	}

	function updateStatus($db){
		$this->Id = htmlspecialchars(strip_tags($this->Id));

		$stmt = $db->prepare("
			UPDATE tasks
			SET 
			Status= '$this->Status',
			UpdatedAt = '$this->UpdatedAt'
			WHERE Id = '$this->Id'");
		if($stmt->execute()){
			return true;
		}
	 
		return false;
	}

	function UpdateRejectStatus($db){
		$this->Id = htmlspecialchars(strip_tags($this->Id));
		$this->Status = htmlspecialchars(strip_tags($this->Status));
		$this->Reason = htmlspecialchars(strip_tags($this->Reason));

		$stmt = $db->prepare("
			UPDATE tasks
			SET 
			Status= '$this->Status',
			UpdatedAt = '$this->UpdatedAt',
			Reason = '$this->Reason'
			WHERE Id = '$this->Id'");
		if($stmt->execute()){
			return true;
		}
	 
		return false;
	}
}
