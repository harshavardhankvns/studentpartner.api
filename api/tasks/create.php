<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
include_once '../Config/Database.php';
include_once '../Classes/Task.php';

$database = new Database();
$db = $database->getConnection();

$task= new Task($db);
$data = json_decode(file_get_contents("php://input"));

if(
   !empty($data->TaskTypeId) &&
   !empty($data->StudentPartnerId) &&
   !empty($data->ConductedOn))
{
    $task->TaskTypeId = $data->TaskTypeId;
    $task->StudentPartnerId = $data->StudentPartnerId;
    $task->ConductedOn = $data->ConductedOn;
    $task->SessionUrl = $data->SessionUrl;
    $task->SessionImageUrl = $data->SessionImageUrl;
    $task->Message = $data->Message;
    $task->CreatedAt = date('Y-m-d H:i:s');
   if($task->create($db))
   {
        http_response_code(201);         
            echo json_encode(array("message" => "Task was created."));
        } 
        else
        {         
            http_response_code(503);        
            echo json_encode(array("message" => "Unable to create Task."));
        }
}

else
{    
            http_response_code(400);    
            echo json_encode(array("message" => "Unable to create Task. Data is incomplete."));
    }
?>
