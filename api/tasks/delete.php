<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
include_once '../Config/Database.php';
include_once '../Classes/Task.php';

$database = new Database();
$db = $database->getConnection();

$task= new Task($db);
$task->Id = $_GET['Id'];

if(!empty($task->Id))
{
	if($task->delete($db))
    {   
        http_response_code(200);         
            echo json_encode(array("message" => "Item was deleted."));
    } 
    else
    {         
        http_response_code(503);        
        echo json_encode(array("message" => "Unable to delete item."));
    }
}
else
{    
    http_response_code(400);    
    echo json_encode(array("message" => "Unable to delete item. Data is incomplete."));
}
