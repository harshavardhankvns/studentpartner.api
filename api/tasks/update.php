<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
include_once '../Config/Database.php';
include_once '../Classes/Task.php';

$database = new Database();
$db = $database->getConnection();

$tasks= new Task($db);
$data = json_decode(file_get_contents("php://input"));

if(!empty($data->Id) &&
   !empty($data->TaskTypeId) &&
   !empty($data->StudentPartnerId) &&
   !empty($data->ConductedOn) &&
   !empty($data->SessionUrl) &&
   !empty($data->SessionImageUrl) &&
   !empty($data->UpdatedAt)){

   $tasks->Id = $data->Id;
    $tasks->TaskTypeId = $data->TaskTypeId;
    $tasks->StudentPartnerId = $data->StudentPartnerId;
    $tasks->ConductedOn = $data->ConductedOn;
    $tasks->SessionUrl = $data->SessionUrl;
    $tasks->SessionImageUrl = $data->SessionImageUrl;
    $tasks->UpdatedAt = date('Y-m-d H:i:s');

   if($tasks->create($db)){
        http_response_code(200);         
            echo json_encode(array("message" => "Item was created."));
        } else{         
            http_response_code(503);        
            echo json_encode(array("message" => "Unable to create item."));
        }
        }else{    
            http_response_code(400);    
            echo json_encode(array("message" => "Unable to create item. Data is incomplete."));
    }
?>
