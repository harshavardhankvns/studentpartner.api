<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
include_once '../Config/Database.php';
include_once '../Classes/Task.php';

$database = new Database();
$db = $database->getConnection();

$task= new Task($db);
$task->StudentPartnerId = $_GET['StudentPartnerId'];

$result = $task->GetByStudentPartnerId($db);
    $taskRecords=array();
    $taskRecords["task"]=array(); 
	while ($task = $result->fetch_assoc()) { 	
        extract($task); 
        $taskDetails=array(
            "Id" => $Id,
            "TaskTypeId" => $TaskTypeId,
            "StudentPartnerId" => $StudentPartnerId,
            "ConductedOn" => $ConductedOn,
            "SessionUrl" => $SessionUrl,
            "SessionImageUrl" => $SessionImageUrl,
            "Message" => $Message,
            "Status" => $Status,
            "Reason" => $Reason,
            "Reward" => $CoinsRewarded,
            "CreatedAt" => $CreatedAt,
            "UpdatedAt" => $UpdatedAt,	
        ); 
       array_push($taskRecords["task"], $taskDetails);
    }    
    http_response_code(200);     
    echo json_encode($taskRecords);
