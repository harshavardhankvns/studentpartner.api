<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../Config/Database.php';
include_once '../Classes/Task.php';
include_once '../Classes/Coin.php';

$database = new Database();
$db = $database->getConnection();

$task = new Task($db);
$coin = new Coin($db);

$data = json_decode(file_get_contents("php://input"));

if (
    !empty($data->TaskTypeId) &&
    !empty($data->StudentPartnerId) &&
    !empty($data->ConductedOn) &&
    !empty($data->Status) &&
    !empty($data->Message)
) {
    $task->TaskTypeId = $data->TaskTypeId;
    $task->StudentPartnerId = $data->StudentPartnerId;
    $task->ConductedOn = date('Y-m-d H:i:s');
    $task->Status = $data->Status;
    $task->Message = $data->Message;
    $coin->CoinsRewarded = $data->CoinsRewarded;
    $task->CreatedAt = date('Y-m-d H:i:s');
    if ($task->createRFToCoins($db)) {
        $result = $task->GetMaxId($db) -> fetch_assoc();
        $coin->taskId = $result["MAX(Id)"];
        $coin->studentPartnerId = $data->StudentPartnerId;
        $coin->coinsRewarded = $data->CoinsRewarded;
        $coin->createdAt = date('Y-m-d H:i:s');

        if ($coin->create($db)) {
            http_response_code(201);
            echo json_encode(array("message" => "Task was created."));
        }
    } else {
        http_response_code(503);
        echo json_encode(array("message" => "Unable to create Task."));
    }
} else {
    http_response_code(400);
    echo json_encode(array("message" => "Unable to create Task. Data is incomplete."));
}
