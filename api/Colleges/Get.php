<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../Config/Database.php';
include_once '../Classes/College.php';

$database = new Database();
$db = $database->getConnection();
 
$college = new College($db);


$result = $college->Get($db);

if($result->num_rows > 0){    
    $collegeRecords=array();
    $collegeRecords["colleges"]=array(); 
	while ($college = $result->fetch_assoc()) { 	
        $collegeDetails=$college; 
       array_push($collegeRecords["colleges"], $collegeDetails);
    }    
    http_response_code(200);     
    echo json_encode($collegeRecords);
}else{     
    http_response_code(404);     
    echo json_encode(
        array("message" => "No college found.")
    );
} 