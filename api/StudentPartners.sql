CREATE TABLE StudentPartners (
	Id INT PRIMARY KEY NOT NULL AUTO_INCREMENT
	,Name VARCHAR(100)
	,ProfilePictureUrl VARCHAR(2000)
	,CollegeName VARCHAR(100)
	,PhoneNumber VARCHAR(15)
	,EmailId VARCHAR(50)
	,LinkedInProfileLink VARCHAR(2000)
	,Password VARCHAR(255)
	,CreatedAt DATETIME
	,UpdatedAt DATETIME
	);

CREATE TABLE Tasks (
    Id int PRIMARY KEY,
    TaskType varchar(200),
    StudentPartnerId INT,
    Name varchar(255),
    SessionUrl varchar(2000),
    SssionImageUrl varchar(2000),
    CreatedAt DATETIME,
    UpdatedAt DATETIME
);