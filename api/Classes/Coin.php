<?php
class Coin
{
    public $id;
    public $taskId;
    public $studentPartnerId;
    public $coinsRewarded;
    public $createdAt;
    public $updatedAt;
    private $coinsTable = "coins";
    private $conn;

    public function __construct($db)
    {
    }
    function create($db){
        $this->taskId = htmlspecialchars(strip_tags($this->taskId));
        $this->studentPartnerId = htmlspecialchars(strip_tags($this->studentPartnerId));
        $this->coinsRewarded = htmlspecialchars(strip_tags($this->coinsRewarded));
        $this->createdAt = htmlspecialchars(strip_tags($this->createdAt));

        $stmt = $db->prepare(
            "INSERT INTO ".$this->coinsTable."(
                   `TaskId`,
                   `studentPartnerId`,
                   `CoinsRewarded`,
                   `CreatedAt`)
                   VALUES(
                       '".$this->taskId."',
                       '".$this->studentPartnerId."',
                       '".$this->coinsRewarded ."',
                       '".$this->createdAt."')");
		
		if($stmt->execute()){
			return true;
		}
	 
		return false;		 
    }
    function GetByStudentPartnerId($db, $studentPartnerId)
    {
        $stmt = $db->prepare("SELECT * FROM " . $this->coinsTable . " C JOIN tasks T ON T.Id = C.TaskId WHERE StudentPartnerId = '$studentPartnerId' ");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    function GetTotalCoinsByStudentPartnerId($db, $studentPartnerId)
    {
        $stmt = $db->prepare("SELECT SUM(CoinsRewarded) AS Coins FROM " . $this->coinsTable . " WHERE StudentPartnerId = $studentPartnerId");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    function GetTotalRedeemedCoinsByStudentPartnerId($db, $studentPartnerId)
    {
        $stmt = $db->prepare("SELECT SUM(Amount) AS RedeemedCoins FROM payout WHERE StudentPartnerId = $studentPartnerId AND Type = 1 AND Status = 1");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    function GetTotalRedeemedAndRequestedCoinsByStudentPartnerId($db, $studentPartnerId)
    {
        $stmt = $db->prepare("SELECT SUM(Amount) AS RedeemedCoins FROM payout WHERE StudentPartnerId = $studentPartnerId AND Type = 1 AND (Status = 1 OR Status = 0)");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    function GetTop($db, $num) {
        $stmt = $db->prepare("SELECT S.*, C.Code AS CollegeName FROM (SELECT SUM(CoinsRewarded) AS Coins,SP.Id, SP.Name, SP.Year, SP.ProfilePictureUrl, SP.CollegeId FROM " . $this->coinsTable . " C RIGHT JOIN studentpartners SP ON SP.Id = C.StudentPartnerId GROUP BY SP.Id ORDER BY SUM(CoinsRewarded) DESC LIMIT " . $num . ") AS S JOIN colleges C ON S.CollegeId = C.Id ORDER BY S.Coins DESC");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }
}
