<?php
class StudentPartner
{
    public $id;
    public $name;
    public $profilePictureUrl;
    public $collegeId;
    public $year;
    public $rollNumber;
    public $phoneNumber;
    public $emailId;
    public $linkedInProfileLink;
    public $uPIId;
    public $password;
    public $createdAt;
    public $updatedAt;

    private $studentPartnersTable = "studentpartners";
    private $conn;

    public function __construct($db)
    {
    }

    function read($db)
    {
        if ($this->id) {
            $stmt = $db->prepare("SELECT * FROM " . $this->studentPartnersTable . " WHERE id = ?");
            $stmt->bind_param("i", $this->id);
        } else {
            $stmt = $db->prepare("SELECT * FROM " . $this->studentPartnersTable);
        }
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }


    function GetByEmailId($db, $email)
    {
        $stmt = $db->prepare("SELECT * FROM " . $this->studentPartnersTable . " WHERE EmailId = '$email'");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    function create($db)
    {

        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->profilePictureUrl = htmlspecialchars(strip_tags($this->profilePictureUrl));
        $this->collegeId = htmlspecialchars(strip_tags($this->collegeId));
        $this->year = htmlspecialchars(strip_tags($this->year));
        $this->rollNumber = htmlspecialchars(strip_tags($this->rollNumber));
        $this->phoneNumber = htmlspecialchars(strip_tags($this->phoneNumber));
        $this->emailId = htmlspecialchars(strip_tags($this->emailId));
        $this->linkedInProfileLink = htmlspecialchars(strip_tags($this->linkedInProfileLink));
        $this->uPIId = htmlspecialchars(strip_tags($this->uPIId));
        $this->password = htmlspecialchars(strip_tags($this->password));
        $this->createdAt = htmlspecialchars(strip_tags($this->createdAt));
        $stmt = $db->prepare(
            "INSERT INTO " . $this->studentPartnersTable . "(
                   `Name`,
                   `ProfilePictureUrl`,
                   `CollegeId`,
                   `Year`,
                   `RollNumber`,
                   `PhoneNumber`,
                   `EmailId`,
                   `LinkedInProfileLink`,
                   `UPIId`,
                   `Password`,
                   `CreatedAt`)
                   VALUES(
                       '" . $this->name . "',
                       '" . $this->profilePictureUrl . "',
                       '" . $this->collegeId . "',
                       '" . $this->year . "',
                       '" . $this->rollNumber . "',
                       '" . $this->phoneNumber . "',
                       '" . $this->emailId . "',
                       '" . $this->linkedInProfileLink . "',
                       '" . $this->uPIId . "',
                       '" . $this->password . "',
                       '" . $this->createdAt . "')"
        );

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    function update($db)
    {
        $this->id = htmlspecialchars(strip_tags($this->id));
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->profilePictureUrl  = htmlspecialchars(strip_tags($this->profilePictureUrl));
        $this->collegeId  = htmlspecialchars(strip_tags($this->collegeId));
        $this->year = htmlspecialchars(strip_tags($this->year));
        $this->rollNumber = htmlspecialchars(strip_tags($this->rollNumber));
        $this->phoneNumber  = htmlspecialchars(strip_tags($this->phoneNumber));
        $this->emailId = htmlspecialchars(strip_tags($this->emailId));
        $this->linkedInProfileLink = htmlspecialchars(strip_tags($this->linkedInProfileLink));
        $this->uPIId = htmlspecialchars(strip_tags($this->uPIId));
        $this->updatedAt = htmlspecialchars(strip_tags($this->updatedAt));

        $sql = "
    UPDATE " . $this->studentPartnersTable . " 
    SET 
    Name = '" . $this->name . "',
    ProfilePictureUrl = '" . $this->profilePictureUrl . "',
    CollegeId = '" . $this->collegeId . "',
    Year = '" . $this->year . "',
    RollNumber = '" . $this->rollNumber . "',
    PhoneNumber = '" . $this->phoneNumber . "',
    EmailId = '" . $this->emailId . "',
    LinkedInProfileLink = '" . $this->linkedInProfileLink . "',
    UPIId = '" . $this->uPIId . "',
    UpdatedAt = '" . $this->updatedAt . "'
    WHERE id = " . $this->id . "";
        $stmt = $db->prepare($sql);


        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    function GetByCollegeId($db, $collegeId) {
        $stmt = $db->prepare("SELECT S.*, C.Name AS CollegeName FROM (SELECT SUM(CoinsRewarded) AS Coins,SP.Id, SP.Name, SP.Year, SP.ProfilePictureUrl, SP.CollegeId FROM coins C RIGHT JOIN studentpartners SP ON SP.Id = C.StudentPartnerId GROUP BY SP.Id ORDER BY SUM(CoinsRewarded) DESC) AS S INNER JOIN colleges C ON S.CollegeId = C.Id WHERE C.Id = ".$collegeId." ORDER BY S.Coins DESC");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }
}
