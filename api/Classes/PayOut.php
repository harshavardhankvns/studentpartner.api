<?php
class PayOut
{
    public $id;
    public $studentPartnerId;
    public $type;
    public $amount;
    public $status;
    public $createdAt;
    public $updatedAt;
    private $payoutTable = "payout";
    private $conn;

    public function __construct($db)
    {
    }

    function GetByStudentPartnerId($db, $studentPartnerId, $type)
    {
        $stmt = $db->prepare("SELECT * FROM " . $this->payoutTable . " WHERE StudentPartnerId = '$studentPartnerId' AND Type = '$type' ORDER BY CreatedAt DESC");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    function create($db)
    {
        $this->studentPartnerId = htmlspecialchars(strip_tags($this->studentPartnerId));
        $this->type = htmlspecialchars(strip_tags($this->type));
        $this->amount = htmlspecialchars(strip_tags($this->amount));
        $this->status = htmlspecialchars(strip_tags($this->status));
        $this->createdAt = htmlspecialchars(strip_tags($this->createdAt));
        $this->updatedAt = htmlspecialchars(strip_tags($this->updatedAt));

        $stmt = $db->prepare(
            "INSERT INTO " . $this->payoutTable . "(
                   `StudentPartnerId`,
                   `Type`,
                   `Amount`,
                   `CreatedAt`)
                   VALUES(
                       '" . $this->studentPartnerId . "',
                       '" . $this->type . "',
                       '" . $this->amount . "',
                       '" . $this->createdAt . "')"
        );

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    function update($db)
    {
        $this->id = htmlspecialchars(strip_tags($this->id));
        $this->studentPartnerId = htmlspecialchars(strip_tags($this->studentPartnerId));
        $this->status = htmlspecialchars(strip_tags($this->status));

        $sql = "
                UPDATE " . $this->payoutTable . " 
                SET 
                StudentPartnerId = '" . $this->studentPartnerId . "',
                Status = '" . $this->status . "',
                UpdatedAt = '" . $this->updatedAt . "'
                WHERE id = " . $this->id . "";
        $stmt = $db->prepare($sql);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }
}
