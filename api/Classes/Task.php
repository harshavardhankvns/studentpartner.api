<?php
class Task{   
    
private $tasksTable = "tasks";
    public $Id;
    public $TaskTypeId;
    public $StudentPartnerId;
    public $ConductedOn;
    public $SessionUrl;
    public $SessionImageUrl;
	public $Message;
	public $Status;
    public $CreatedAt;
    public $UpdatedAt;
    private $conn;
    
    public function __construct($db){
    }	
    
	function read($db){	
		if($this->Id) {
    $stmt = $db->prepare("SELECT * FROM ".$this->tasksTable." WHERE Id = '$this->Id'");
    } else {
    $stmt = $db->prepare("SELECT * FROM ".$this->tasksTable);
    
    }		
		$stmt->execute();			
		$result = $stmt->get_result();		
		return $result;	
	}
    
	function GetByStudentPartnerId($db){	
    $stmt = $db->prepare("SELECT T.*, C.CoinsRewarded FROM ".$this->tasksTable." T LEFT JOIN coins C ON T.Id = C.TaskId WHERE T.StudentPartnerId = '$this->StudentPartnerId' ORDER BY T.CreatedAt DESC");
		$stmt->execute();			
		$result = $stmt->get_result();		
		return $result;	
	}

   function create($db){
		$this->TaskTypeId = htmlspecialchars(strip_tags($this->TaskTypeId));
		$this->StudentPartnerId = htmlspecialchars(strip_tags($this->StudentPartnerId));
		$this->ConductedOn = htmlspecialchars(strip_tags($this->ConductedOn));
		$this->SessionUrl = htmlspecialchars(strip_tags($this->SessionUrl));
		$this->SessionImageUrl = htmlspecialchars(strip_tags($this->SessionImageUrl));
		$this->Message = htmlspecialchars(strip_tags($this->Message));
		$this->CreatedAt = htmlspecialchars(strip_tags($this->CreatedAt));

    $stmt = $db->prepare(
     "INSERT INTO ".$this->tasksTable."(
			`TaskTypeId`,
			`StudentPartnerId`,
			`ConductedOn`,
			`SessionUrl`,
			`SessionImageUrl`,
			`Message`,
			`CreatedAt`)
			VALUES(
				'$this->TaskTypeId',
				'$this->StudentPartnerId',
				'$this->ConductedOn',
				'$this->SessionUrl',
				'$this->SessionImageUrl',
				'$this->Message',
				'$this->CreatedAt')");
		
		
		if($stmt->execute()){
			return true;
		}
	 
		return false;		 
	}
		
	function update($db){

    $this->Id = htmlspecialchars(strip_tags($this->Id));
		$this->TaskTypeId = htmlspecialchars(strip_tags($this->TaskTypeId));
		$this->StudentPartnerId = htmlspecialchars(strip_tags($this->StudentPartnerId));
		$this->ConductedOn = htmlspecialchars(strip_tags($this->ConductedOn));
		$this->SessionUrl = htmlspecialchars(strip_tags($this->SessionUrl));
		$this->SessionImageUrl = htmlspecialchars(strip_tags($this->SessionImageUrl));
		$this->CreatedAt = htmlspecialchars(strip_tags($this->CreatedAt));
		$this->UpdatedAt = htmlspecialchars(strip_tags($this->UpdatedAt));
		$this->created = htmlspecialchars(strip_tags($this->created));

		$stmt = $db->prepare("
			UPDATE ".$this->tasksTable." 
			SET 
			TaskTypeId= '$this->TaskTypeId',
			StudentPartnerId= '$this->StudentPartnerId',
			ConductedOn= '$this->ConductedOn',
			SessionUrl= '$this->SessionUrl',
			SessionImageUrl= '$this->SessionImageUrl',
			CreatedAt= '$this->CreatedAt',
			UpdatedAt= '$this->UpdatedAt',
			category_Id = ' $this->category_Id', 
			created = '$this->created'
			WHERE Id = '$this->Id'");
	 
		
		if($stmt->execute()){
			return true;
		}
	 
		return false;
	}
	
	function delete($db){
		
		$this->Id = htmlspecialchars(strip_tags($this->Id));


		$stmt = $db->prepare("
			DELETE FROM ".$this->tasksTable." 
			WHERE Id = '$this->Id'");
				 
		if($stmt->execute()){
			return true;
		}
	 
		return false;		 
	}

	function createRFToCoins($db){
		$this->TaskTypeId = htmlspecialchars(strip_tags($this->TaskTypeId));
		$this->StudentPartnerId = htmlspecialchars(strip_tags($this->StudentPartnerId));
		$this->ConductedOn = htmlspecialchars(strip_tags($this->ConductedOn));
		$this->Status = htmlspecialchars(strip_tags($this->Status));
		$this->Message = htmlspecialchars(strip_tags($this->Message));
		$this->CreatedAt = htmlspecialchars(strip_tags($this->CreatedAt));

    $stmt = $db->prepare(
     "INSERT INTO ".$this->tasksTable."(
			`TaskTypeId`,
			`StudentPartnerId`,
			`ConductedOn`,
			`Status`,
			`Message`,
			`CreatedAt`)
			VALUES(
				'$this->TaskTypeId',
				'$this->StudentPartnerId',
				'$this->ConductedOn',
				'$this->Status',
				'$this->Message',
				'$this->CreatedAt')");
		
		
		if($stmt->execute()){
			return true;
		}
	 
		return false;		 
	}

	function GetMaxId($db) {
		$stmt = $db->prepare("SELECT MAX(Id) FROM tasks");
		$stmt->execute();			
		$result = $stmt->get_result();		
		return $result;	
	}

	
    function GetRedeemedReferralPointsByStudentPartnerId($db, $studentPartnerId)
    {
        $stmt = $db->prepare("SELECT SUM(C.CoinsRewarded) AS RedeemedReferralPoints FROM tasks T JOIN coins C ON C.TaskId = T.Id  WHERE T.StudentPartnerId = $studentPartnerId AND T.TaskTypeId = 'Convert to coins'");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }
}
