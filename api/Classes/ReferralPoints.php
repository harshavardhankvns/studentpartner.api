<?php
class ReferralPoints
{
    public $id;
    public $studentPartnerId;
    public $studentId;
    public $totalCost;
    public $referralCommission;
    public $updatedAt;
    public $createdAt;
    private $ReferralPointsTable = "referralpoints";
    private $conn;

    public function __construct($db)
    {
    }
    function create($db)
    {
        $this->totalCost = htmlspecialchars(strip_tags($this->totalCost));
        $this->studentPartnerId = htmlspecialchars(strip_tags($this->studentPartnerId));
        $this->referralComission = htmlspecialchars(strip_tags($this->referralComission));
        $this->createdAt = htmlspecialchars(strip_tags($this->createdAt));

        $stmt = $db->prepare(
            "INSERT INTO " . $this->ReferralPointsTable . "(
                   `TotalCost`,
                   `StudentPartnerId`,
                   `ReferralComission`,
                   `CreatedAt`)
                   VALUES(
                       '" . $this->totalCost . "',
                       '" . $this->studentPartnerId . "',
                       '" . $this->referralComission . "',
                       '" . $this->createdAt . "')"
        );

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    function GetTotalReferralPointsByStudentPartnerId($db, $studentPartnerId)
    {
        $stmt = $db->prepare("SELECT SUM(ReferralCommission) AS ReferralPoints FROM " . $this->ReferralPointsTable . " WHERE StudentPartnerId = $studentPartnerId");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    function GetTotalAvailableReferralPointsByStudentPartnerId($db, $studentPartnerId)
    {
        $stmt = $db->prepare("SELECT SUM(Amount) AS AvailableReferralPoints FROM payout WHERE StudentPartnerId = $studentPartnerId AND Type = 2 AND Status != 1");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }


    function GetByStudentPartnerId($db, $studentPartnerId)
    {
        $stmt = $db->prepare("SELECT * FROM " . $this->ReferralPointsTable . " WHERE StudentPartnerId = " . $studentPartnerId . ";");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    function GetCountByStudentPartnerId($db, $studentPartnerId)
    {
        $stmt = $db->prepare("SELECT COUNT(*) AS ReferralCount FROM " . $this->ReferralPointsTable . " WHERE StudentPartnerId = " . $studentPartnerId . ";");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }
}
