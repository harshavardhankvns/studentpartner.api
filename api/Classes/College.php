<?php
class College
{
    public $Id;
    public $Name;
    public $Code;
    public $University;
    public $createdAt;
    public $updatedAt;
    private $collegeTable = "colleges";
    private $conn;

    public function __construct($db)
    {
    }

    function Get($db)
    {
        $stmt = $db->prepare("SELECT * FROM " . $this->collegeTable . "");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    function GetHome($db)
    {
        $stmt = $db->prepare("SELECT C.* FROM colleges C INNER JOIN studentpartners SP ON SP.CollegeId = C.Id GROUP BY C.Id");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }
}
?>