<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../Config/Database.php';
include_once '../Classes/studentPartner.php';

$database = new Database();
$db = $database->getConnection();
 
$studentPartner = new studentPartner($db);

$studentPartner->id = (isset($_GET['id']) && $_GET['id']) ? $_GET['id'] : '0';

$result = $studentPartner->read($db);

if($result->num_rows > 0){    
    $studentPartnerRecords=array();
    $studentPartnerRecords["studentPartners"]=array(); 
	while ($studentPartner = $result->fetch_assoc()) { 	
        $studentPartnerDetails=array($studentPartner); 
       array_push($studentPartnerRecords["studentPartners"], $studentPartnerDetails);
    }    
    http_response_code(200);     
    echo json_encode($studentPartnerRecords);
}else{     
    http_response_code(404);     
    echo json_encode(
        array("message" => "No studentPartner found.")
    );
} 