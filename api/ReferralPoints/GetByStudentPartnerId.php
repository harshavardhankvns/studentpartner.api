<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../Config/Database.php';
include_once '../Classes/ReferralPoints.php';
include_once '../Classes/Task.php';

$database = new Database();
$db = $database->getConnection();

$referralPoints = new ReferralPoints($db);
$task = new Task($db);

$studentPartnerId = $_GET['StudentPartnerId'];
$result = $referralPoints->GetTotalReferralPointsByStudentPartnerId($db, $studentPartnerId);
$result1 = $task->GetRedeemedReferralPointsByStudentPartnerId($db, $studentPartnerId);
http_response_code(200);
$totalReferralPoints = $result->fetch_assoc();
extract($totalReferralPoints); 
$redeemedReferralPoints = $result1->fetch_assoc();
extract($redeemedReferralPoints); 

echo json_encode(array("ReferralPoints" => $ReferralPoints + 0, "AvailableReferralPoints" => $ReferralPoints - $RedeemedReferralPoints));
