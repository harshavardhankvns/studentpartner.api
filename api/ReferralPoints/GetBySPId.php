<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../Config/Database.php';
include_once '../Classes/ReferralPoints.php';

$database = new Database();
$db = $database->getConnection();

$referralPoints = new ReferralPoints($db);
$studentPartnerId = $_GET['StudentPartnerId'];

$result = $referralPoints->GetByStudentPartnerId($db, $studentPartnerId);
$referralPointsRecords = array();
while ($referralPoints = $result->fetch_assoc()) {
    extract($referralPoints);
    $referralPointsDetails = array(
        "Id" => $Id,
        "StudentId" => $StudentId,
        "StudentPartnerId" => $StudentPartnerId,
        "TotalCost" => $TotalCost,
        "ReferralCommission" => $ReferralCommission,
        "CreatedAt" => $CreatedAt,
        "UpdatedAt" => $UpdatedAt,
    );
    array_push($referralPointsRecords, $referralPointsDetails);
}
http_response_code(200);
echo json_encode($referralPointsRecords);
