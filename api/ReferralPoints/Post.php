<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../Config/Database.php';
include_once '../Classes/Coin.php';

$database = new Database();
$db = $database->getConnection();

$coin = new Coin($db);

$data = json_decode(file_get_contents("php://input"));

if (
    !empty($data->TaskId) &&
    !empty($data->StudentPartnerId) &&
    !empty($data->CoinsRewarded)
) {
    $coin->taskId = $data->TaskId;
    $coin->studentPartnerId = $data->StudentPartnerId;
    $coin->coinsRewarded = $data->CoinsRewarded;
    $coin->createdAt = date('Y-m-d H:i:s');

    if ($coin->create($db)) {
        http_response_code(201);
        echo json_encode(array("message" => "coin was created."));
    } else {
        http_response_code(503);
        echo json_encode(array("message" => "Unable to create coin."));
    }
} else {
    http_response_code(400);
    echo json_encode(array("message" => "Unable to create coin. Data is incomplete."));
}
