<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../Config/Database.php';
include_once '../Classes/PayOut.php';

$database = new Database();
$db = $database->getConnection();

$payout = new Payout($db);

$data = json_decode(file_get_contents("php://input"));

if (
    !empty($data->StudentPartnerId) &&
    !empty($data->Type) &&
    !empty($data->Amount)
) {
    $payout->studentPartnerId = $data->StudentPartnerId;
    $payout->type = $data->Type;
    $payout->amount = $data->Amount;
    $payout->createdAt = date('Y-m-d H:i:s');

    if ($payout->create($db)) {
        http_response_code(201);
        echo json_encode(array("message" => "payout was created."));
    } else {
        http_response_code(503);
        echo json_encode(array("message" => "Unable to create payout."));
    }
} else {
    http_response_code(400);
    echo json_encode(array("message" => "Unable to create payout. Data is incomplete."));
}
