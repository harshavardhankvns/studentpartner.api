<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../Config/Database.php';
include_once '../Classes/PayOut.php';

$database = new Database();
$db = $database->getConnection();

$payout = new Payout($db);

$data = json_decode(file_get_contents("php://input"));

if (
    !empty($data->Id) &&
    !empty($data->StudentPartnerId) &&
    !empty($data->Status)
) {
    $payout->id = $data->Id;
    $payout->studentPartnerId = $data->StudentPartnerId;
    $payout->status = $data->Status;
    $payout->updatedAt = date('Y-m-d H:i:s');

    if ($payout->update($db)) {
        http_response_code(201);
        echo json_encode(array("message" => "payout was updated."));
    } else {
        http_response_code(503);
        echo json_encode(array("message" => "Unable to update payout."));
    }
} else {
    http_response_code(400);
    echo json_encode(array("message" => "Unable to update payout. Data is incomplete."));
}
