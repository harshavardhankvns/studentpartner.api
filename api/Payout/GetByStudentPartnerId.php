<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
include_once '../Config/Database.php';
include_once '../Classes/PayOut.php';

$database = new Database();
$db = $database->getConnection();

$payout= new PayOut($db);

$result = $payout->GetByStudentPartnerId($db, $_GET['StudentPartnerId'], $_GET['Type']);
    $payouts=array();
    $payouts["payouts"]=array(); 
	while ($payout = $result->fetch_assoc()) { 	
        extract($payout); 
        $payout=array(
            "Id" => $Id,
            "StudentPartnerId" => $StudentPartnerId,
            "Type" => $Type,
            "Amount" => $Amount,
            "Status" => $Status,
            "CreatedAt" => $CreatedAt,
            "UpdatedAt" => $UpdatedAt
        ); 
       array_push($payouts["payouts"], $payout);
    }    
    http_response_code(200);     
    echo json_encode($payouts);
