<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../Config/Database.php';
include_once '../Classes/Coin.php';
include_once '../Classes/ReferralPoints.php';

$database = new Database();
$db = $database->getConnection();

$coin = new Coin($db);
$referralPoints = new ReferralPoints($db);

$studentPartnerId = $_GET['StudentPartnerId'];
$result = $coin->GetTotalCoinsByStudentPartnerId($db, $studentPartnerId);
$result1 = $coin->GetTotalRedeemedCoinsByStudentPartnerId($db, $studentPartnerId);
$result2 = $referralPoints->GetCountByStudentPartnerId($db, $studentPartnerId);
$result3 = $referralPoints->GetTotalReferralPointsByStudentPartnerId($db, $studentPartnerId);

http_response_code(200);
$totalCoins = $result->fetch_assoc();
extract($totalCoins);
$redeemedCoins = $result1->fetch_assoc();
extract($redeemedCoins);
$referralCount = $result2->fetch_assoc();
extract($referralCount);
$referralPointsEarned = $result3->fetch_assoc();
extract($referralPointsEarned);

echo json_encode(array("Coins" => $Coins + 0, "AvailableCoins" => $Coins - $RedeemedCoins, "RedeemedCoins" => $RedeemedCoins - 0 , "ReferralCount" => $ReferralCount - 0, "ReferralPointsEarned" => $ReferralPoints-0));
