<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../Config/Database.php';
include_once '../Classes/StudentPartner.php';

$database = new Database();
$db = $database->getConnection();
 
$studentPartner = new StudentPartner($db);

$studentPartner->id = (isset($_GET['Id']) && $_GET['Id']) ? $_GET['Id'] : '0';

$result = $studentPartner->read($db);

if($result->num_rows > 0){    
    $studentPartnerRecords=array();
    $studentPartnerRecords["studentPartners"]=array(); 
	while ($studentPartner = $result->fetch_assoc()) { 	
        $studentPartnerDetails=array($studentPartner); 
       array_push($studentPartnerRecords["studentPartners"], $studentPartnerDetails);
    }    
    http_response_code(200);     
    echo json_encode($studentPartnerRecords);
}else{     
    http_response_code(404);     
    echo json_encode(
        array("message" => "No studentPartner found.")
    );
} 