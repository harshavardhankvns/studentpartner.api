<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../Config/Database.php';
include_once '../Classes/StudentPartner.php';

$database = new Database();
$db = $database->getConnection();

$studentPartner = new StudentPartner($db);

$data = json_decode(file_get_contents("php://input"));

if(
    !empty($data->Id) &&
    !empty($data->Name) &&
    !empty($data->ProfilePictureUrl ) &&
    !empty($data->CollegeId ) &&
    !empty($data->Year) &&
    !empty($data->RollNumber ) &&
    !empty($data->UPIId) &&
    !empty($data->PhoneNumber ) &&
    !empty($data->EmailId) ) {

    $studentPartner->id = $data->Id;
    $studentPartner->name = $data->Name;
    $studentPartner->profilePictureUrl = $data->ProfilePictureUrl;
    $studentPartner->collegeId = $data->CollegeId;
    $studentPartner->year = $data->Year;
    $studentPartner->phoneNumber = $data->PhoneNumber;
    $studentPartner->emailId = $data->EmailId;
    $studentPartner->rollNumber = $data->RollNumber;
    $studentPartner->uPIId = $data->UPIId;
    $studentPartner->linkedInProfileLink = $data->LinkedInProfileLink;
    $studentPartner->updatedAt = date('Y-m-d H:i:s');
    if ($studentPartner->update($db)) {
        http_response_code(200);
        echo json_encode(array("message" => "Student Partner was updated."));
    } else {
        http_response_code(503);
        echo json_encode(array("message" => "Unable to update Student Partner."));
    }
} else {
    http_response_code(400);
    echo json_encode(array("message" => "Unable to update Student Partner. Data is incomplete."));
}
