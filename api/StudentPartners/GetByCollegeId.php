<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../Config/Database.php';
include_once '../Classes/StudentPartner.php';

$database = new Database();
$db = $database->getConnection();
 
$studentPartner = new StudentPartner($db);

$collegeId = $_GET['CollegeId'];

$result = $studentPartner->GetByCollegeId($db, $collegeId);

$topRecords=array();
while ($top = $result->fetch_assoc()) {
    extract($top); 
   array_push($topRecords, array("Coins" => $Coins + 0, "Name" => $Name, "ProfilePictureUrl" => $ProfilePictureUrl, "Year" => $Year));
}    
http_response_code(200);

echo json_encode($topRecords);