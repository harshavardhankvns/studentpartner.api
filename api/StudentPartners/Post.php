<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../Config/Database.php';
include_once '../Classes/StudentPartner.php';

$database = new Database();
$db = $database->getConnection();

$studentPartner = new StudentPartner($db);

$data = json_decode(file_get_contents("php://input"));

if (
    !empty($data->Name) &&
    !empty($data->ProfilePictureUrl) &&
    !empty($data->CollegeId) &&
    !empty($data->Year) &&
    !empty($data->RollNumber) &&
    !empty($data->PhoneNumber) &&
    !empty($data->UPIId) &&
    !empty($data->EmailId) &&
    !empty($data->Password)
) {
    $studentPartner->name = $data->Name;
    $studentPartner->profilePictureUrl = $data->ProfilePictureUrl;
    $studentPartner->collegeId = $data->CollegeId;
    $studentPartner->year = $data->Year;
    $studentPartner->rollNumber = $data->RollNumber;
    $studentPartner->phoneNumber = $data->PhoneNumber;
    $studentPartner->emailId = $data->EmailId;
    $studentPartner->linkedInProfileLink = $data->LinkedInProfileLink;
    $studentPartner->uPIId = $data->UPIId;
    $studentPartner->password = $data->Password;
    $studentPartner->createdAt = date('Y-m-d H:i:s');

    $result = $studentPartner->GetByEmailId($db, $data->EmailId);
    if ($result->num_rows > 0) {
        http_response_code(409);
        echo json_encode(array("message" => "Email Id Already exists"));
    } else {
        if ($studentPartner->create($db)) {
            http_response_code(201);
            echo json_encode(array("message" => "studentPartner was created."));
        } else {
            http_response_code(503);
            echo json_encode(array("message" => "Unable to create studentPartner."));
        }
    }
} else {
    http_response_code(400);
    echo json_encode(array("message" => "Unable to create studentPartner. Data is incomplete."));
}
