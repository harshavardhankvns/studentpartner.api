<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../Config/Database.php';
include_once '../Classes/Coin.php';

$database = new Database();
$db = $database->getConnection();

$coin = new Coin($db);
$num = $_GET['top'];
$result = $coin->GetTop($db, $num);
$topRecords=array();
	while ($top = $result->fetch_assoc()) { 	
        extract($top); 
       array_push($topRecords, array(
           "Coins" => $Coins + 0, 
           "Name" => $Name, 
            "ProfilePictureUrl" => $ProfilePictureUrl, 
            "CollegeName" => $CollegeName,
            "Year" => $Year,
            "Id" => $Id
    ));
    }    
http_response_code(200);

echo json_encode($topRecords);
