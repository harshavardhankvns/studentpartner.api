<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../Config/Database.php';
include_once '../Classes/Coin.php';

$database = new Database();
$db = $database->getConnection();

$coin = new Coin($db);
$studentPartnerId = $_GET['StudentPartnerId'];
$result = $coin->GetTotalCoinsByStudentPartnerId($db, $studentPartnerId);
$result1 = $coin->GetTotalRedeemedAndRequestedCoinsByStudentPartnerId($db, $studentPartnerId);
http_response_code(200);
$totalCoins = $result->fetch_assoc();
extract($totalCoins); 
$redeemedCoins = $result1->fetch_assoc();
extract($redeemedCoins); 

echo json_encode(array("Coins" => $Coins + 0, "AvailableCoins" => $Coins - $RedeemedCoins));
